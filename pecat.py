from Crypto.Cipher import AES, DES3
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
from base64 import b64encode
from Crypto.Util.Padding import pad
from Crypto.Random import get_random_bytes
from Crypto.Signature import pkcs1_15
from Crypto.Hash import SHA3_256, SHA3_512
from textwrap import wrap

def read_config(config_filename):
    config_dict = {}
    with open(config_filename, "r") as config_file:
        lines = config_file.readlines()
        for line in lines:
            line = line.split(":")
            line = [w.strip() for w in line]
            config_dict[line[0]] = line[1]

    return config_dict

def generate_secret_key(sym_alg, sym_alg_type):
    if sym_alg == "des3":
        if sym_alg_type == "1":  # option 1: k1, k2, k3 (all different)
            k1 = get_random_bytes(8)
            k2 = get_random_bytes(8)
            k3 = get_random_bytes(8)
            secret_key = k1 + k2 + k3
            print("K1 =", k1.hex())
            print("K2 =", k2.hex())
            print("K3 =", k3.hex())
            print("K(DES3) =", secret_key.hex())
        elif sym_alg_type == "2":   # option 2: k1, k2, k3 (k1=k3)
            k1 = get_random_bytes(8)
            k2 = get_random_bytes(8)
            secret_key = k1 + k2 + k1
            print("K1 =", k1.hex())
            print("K2 =", k2.hex())
            print("K(DES3) =", secret_key.hex())
        else:   # "3"   # option 3: k1, k2, k3 (k1=k2=k3)
            raise ValueError("Cannot have all 3 keys same. Triple DES key degenerates to single DES.")
    else: # "aes"
        if sym_alg_type == "128":
            secret_key = get_random_bytes(16)
        elif sym_alg_type == "192":
            secret_key = get_random_bytes(24)
        else:  # "256":
            secret_key = get_random_bytes(32)

        print("K(AES) =", secret_key.hex())

    return secret_key

def generate_public_private_keys(rsa_keys_len, public_exp_int):
    key = RSA.generate(rsa_keys_len, e=public_exp_int)
    private_exp_int = key.d
    modulus_int = key.n

    public_exp = public_exp_int.to_bytes(length=3, byteorder="big")
    private_exp = private_exp_int.to_bytes(length=rsa_keys_len, byteorder="big")
    modulus = modulus_int.to_bytes(length=rsa_keys_len, byteorder="big")

    print("e (public exponent) =", public_exp.hex())
    print("d (private exponent) =")
    wrapped_txt = wrap(private_exp.hex(), width=60)
    for x in wrapped_txt:
        print(x)

    print("n (modulus) =")
    wrapped_txt = wrap(modulus.hex(), width=60)
    for x in wrapped_txt:
        print(x)
    print()
    print("public key = (e, n) = ({}, \n{})".format(public_exp.hex(), modulus.hex()))
    print("private key = (d, n) = ({}, \n{})".format(private_exp.hex(), modulus.hex()))
    print()

    constr_key_public = RSA.construct((modulus_int, public_exp_int), consistency_check=True)
    constr_key_private = RSA.construct((modulus_int, public_exp_int, private_exp_int), consistency_check=True)

    return constr_key_public, constr_key_private




def generate_keys(config_dict):
    sym_alg, sym_alg_type, sym_mode, sender_rsa_keys_len, receiver_rsa_keys_len, sender_public_exp, receiver_public_exp = config_dict["sym_alg"], config_dict["sym_alg_type"], config_dict["sym_mode"], \
                                                                                    int(config_dict["sender_rsa_keys_len"]), int(config_dict["receiver_rsa_keys_len"]), \
                                                                                    int(config_dict["sender_public_exp"]), int(config_dict["receiver_public_exp"]), \


    print()
    print()
    print("KEYS")
    print("symmetric:")
    secret_key = generate_secret_key(sym_alg, sym_alg_type)


    print()
    print("asymmetric:")
    print(":::: A (sender) ::::")
    A_constr_key_public, A_constr_key_private = generate_public_private_keys(sender_rsa_keys_len, sender_public_exp)
    print()
    print(":::: B (receiver) ::::")
    B_constr_key_public, B_constr_key_private = generate_public_private_keys(receiver_rsa_keys_len, receiver_public_exp)
    print()

    return secret_key, (A_constr_key_public, A_constr_key_private), (B_constr_key_public, B_constr_key_private)


def read_input_file(in_file_filename):
    with open(in_file_filename) as file_txt:
        txt = file_txt.readlines()[0]
        print("in file txt =")
        wrapped_txt = wrap(txt,width=60)
        for x in wrapped_txt:
            print(x)
        txt = bytes(txt, "utf-8")
        print()

    return txt


def b64_encode_input_file(txt):
    txt_b64_enc = b64encode(txt)
    print("in file txt (b64 encoded) =")
    wrapped_txt = wrap(str(txt_b64_enc,encoding="utf-8"), width=60)
    for x in wrapped_txt:
        print(x)
    print()

    return txt_b64_enc

def pad_txt_if_needed(txt_b64_enc, sym_alg, sym_mode):
    block_size = DES3.block_size if sym_alg == "des3" else AES.block_size
    if sym_mode == "ECB" or sym_mode == "CBC":  # other modes (CFB, OFB, CTR do not need input txt padded to fixed length)
        txt_b64_enc = pad(txt_b64_enc, block_size)

    return txt_b64_enc

def get_symmetric_algo_obj(secret_key, sym_alg, sym_mode, iv=None):
    if sym_alg == "des3":   # DES3.block_size = 8
        if sym_mode == "ECB":
            return DES3.new(secret_key, mode=DES3.MODE_ECB)
        elif sym_mode == "CBC":
            if not iv:
                iv = get_random_bytes(DES3.block_size)
                print("IV (initialization vector) =", iv.hex())
            return DES3.new(secret_key, mode=DES3.MODE_CBC, iv=iv), iv
        elif sym_mode == "CFB":
            if not iv:
                iv = get_random_bytes(DES3.block_size)
                print("IV (initialization vector) =", iv.hex())
            return DES3.new(secret_key, mode=DES3.MODE_CFB, iv=iv), iv
        elif sym_mode == "OFB":
            if not iv:
                iv = get_random_bytes(DES3.block_size)
                print("IV (initialization vector) =", iv.hex())
            return DES3.new(secret_key, mode=DES3.MODE_OFB, iv=iv), iv
        else:   # "CTR"
            if not iv:
                iv = get_random_bytes(DES3.block_size)
                nonce = iv[:DES3.block_size // 2]
                print("IV (initialization vector) =", nonce.hex())
            else:
                nonce = iv
            return DES3.new(secret_key, mode=DES3.MODE_CTR, nonce=nonce), nonce
    else:   # "aes"     # AES.block_size = 16
        if sym_mode == "ECB":
            return AES.new(secret_key, mode=AES.MODE_ECB)
        elif sym_mode == "CBC":
            if not iv:
                iv = get_random_bytes(AES.block_size)
                print("IV (initialization vector) =", iv.hex())
            return AES.new(secret_key, mode=AES.MODE_CBC, iv=iv), iv
        elif sym_mode == "CFB":
            if not iv:
                iv = get_random_bytes(AES.block_size)
                print("IV (initialization vector) =", iv.hex())
            return AES.new(secret_key, mode=AES.MODE_CFB, iv=iv), iv
        elif sym_mode == "OFB":
            if not iv:
                iv = get_random_bytes(AES.block_size)
                print("IV (initialization vector) =", iv.hex())
            return AES.new(secret_key, mode=AES.MODE_OFB, iv=iv), iv
        else:  # "CTR"
            if not iv:
                iv = get_random_bytes(AES.block_size)
                nonce = iv[:DES3.block_size // 2]
                print("IV (initialization vector) =", nonce.hex())
            else:
                nonce = iv
            return AES.new(secret_key, mode=AES.MODE_CTR, nonce=nonce), nonce

def get_hash_fun_obj(sha3_hash_fun_type):
    if sha3_hash_fun_type == "256":  # SHA3-256
        return SHA3_256.new()
    else:   # "512"       # SHA3-512
        return  SHA3_512.new()


if __name__=="__main__":
    config_dict = read_config("config.txt")

    txt = read_input_file(config_dict["filename"])
    txt_b64_enc = b64_encode_input_file(txt)

    # gen. secret key for symmetric algo (AES/DES3)
    # A: gen. public-private key pair
    # B: gen. public-private key pairs
    secret_key, A_constr_keys, B_constr_keys  = generate_keys(config_dict)
    A_constr_key_public, A_constr_key_private = A_constr_keys
    B_constr_key_public, B_constr_key_private = B_constr_keys



    print()
    print()
    print("###############################\n############## A ##############\n###############################")

    print()
    print("A: encrypt message with symmetric algo (AES/DES3)")
    txt_b64_enc = pad_txt_if_needed(txt_b64_enc, config_dict["sym_alg"], config_dict["sym_mode"])
    if config_dict["sym_mode"] == "ECB":
        sym_algo_obj = get_symmetric_algo_obj(secret_key, config_dict["sym_alg"], config_dict["sym_mode"])
    else:
        sym_algo_obj, init_vector = get_symmetric_algo_obj(secret_key, config_dict["sym_alg"], config_dict["sym_mode"])

    sym_enc = sym_algo_obj.encrypt(txt_b64_enc)
    C1 = sym_enc
    print("C1 (encrypted message) =")
    C1_wrap_txt = wrap(C1.hex())
    for x in C1_wrap_txt:
        print(x)

    print()
    print("A: encrypt (symmetric) secret key message with asymmetric algo (RSA) - with public key of B")
    rsa = PKCS1_OAEP.new(B_constr_key_public)
    rsa_enc = rsa.encrypt(secret_key)
    C2 = rsa_enc
    print("C2 (encrypted symmetric key) =")
    C2_wrap_txt = wrap(C2.hex())
    for x in C2_wrap_txt:
        print(x)

    envelope = C1 + C2
    print("digital envelope = C1 + C2 =")
    envelope_wrap_txt = wrap(envelope.hex())
    for x in envelope_wrap_txt:
        print(x)


    print()
    print("A: sign envelope - encrypt (hashed) envelope with asymmetric algo (RSA) - with private key of A")
    envelope_signer = pkcs1_15.new(A_constr_key_private)
    hasher = get_hash_fun_obj(config_dict["sha3_type"])
    hasher.update(envelope)
    envelope_signature = envelope_signer.sign(msg_hash=hasher)

    C3 = envelope_signature
    print("C3 (envelope signature) =")
    C3_wrap_txt = wrap(C3.hex())
    for x in C3_wrap_txt:
        print(x)


    print()
    seal = C1 + C2 + C3
    print("digital seal = C1 + C2 + C3 =", seal.hex())
    seal_wrap_txt = wrap(seal.hex())
    for x in seal_wrap_txt:
        print(x)


    print()
    print()
    print("###############################\n############## B ##############\n###############################")

    print()
    print("B: decrypt encrypted (symmetric) secret key message with asymmetric algo (RSA) - with private key of B")
    rsa = PKCS1_OAEP.new(B_constr_key_private)
    secret_key_dec = rsa.decrypt(C2)
    print("K_decrypted =")
    secret_key_dec_wrap_txt = wrap(secret_key_dec.hex())
    for x in secret_key_dec_wrap_txt:
        print(x)
    print("K_decrypted == K:", secret_key_dec == secret_key)

    print()
    print("B: decrypt encrypted message with symmetric algo (AES/DES3)")
    if config_dict["sym_mode"] == "ECB":
        sym_algo_obj = get_symmetric_algo_obj(secret_key_dec, config_dict["sym_alg"], config_dict["sym_mode"])
    else:
        sym_algo_obj, _ = get_symmetric_algo_obj(secret_key_dec, config_dict["sym_alg"], config_dict["sym_mode"], iv=init_vector)

    message_dec = sym_algo_obj.decrypt(C1)
    print("M_decrypted =")
    message_dec_wrap_txt = wrap(message_dec.hex())
    for x in message_dec_wrap_txt:
        print(x)
    print("M_decrypted == M:", message_dec == txt_b64_enc)

    print()
    print("B: decrypt envelope signature with asymmetric algo (RSA) - with public key of A")
    envelope_sign_verifier = pkcs1_15.new(A_constr_key_public)
    hasher = get_hash_fun_obj(config_dict["sha3_type"])
    hasher.update(envelope)
    print("Signature is: ", end="")
    try:
        envelope_sign_verifier.verify(msg_hash=hasher,signature=C3)
        print("OK (hash(envelope) == signature)")
    except ValueError:
        print("invalid (hash(envelope) != signature)!")

