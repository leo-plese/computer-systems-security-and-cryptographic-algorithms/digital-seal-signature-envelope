Digital Seal - digitally signed digital envelope. Implemented in Python using Crypto.Cipher (symmetric cryptoalgorithms AES and DES3), Crypto.PublicKey (asymmetric cryptoalgorithm RSA), Crypto.Hash (hash functions SHA3-256 and SHA3-512) and other packages from Crypto.

My lab assignment in Advanced Operating Systems, FER, Zagreb.

Task description in "TaskSpecification.pdf".

Created: 2021
